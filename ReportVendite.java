package corso.lez5.esercizioscarpe;

import java.util.ArrayList;

//Report Vendite raggruppa tutte le vendite in una lista come un registro di cassa
public class ReportVendite {

	private ArrayList<Vendita> reportVendite = new ArrayList<Vendita>();
	
	public void inserisciVendita (Vendita vendita) {
		this.reportVendite.add(vendita);
	}
	
	public void rimuoviVendita (Vendita vendita) {
		this.reportVendite.remove(vendita);
	}
	
	public void stampaReportVendite() { 
		System.out.println("\n--REPORT VENDITE--\n");
		for (int i = 0; i < reportVendite.size(); i++) {
			
			System.out.println(reportVendite.get(i).getData() + " / " + reportVendite.get(i).getPrezzo()
					+ "$ / " + reportVendite.get(i).getCode());
		}
	}
	
	public void totaleIncasso() { 
		int conto = 0;
		for (int i = 0; i < reportVendite.size(); i++) {
			conto+=reportVendite.get(i).getPrezzo();
		}
		System.out.println("\nIncasso totale: " + conto +"$.");
	}
	
	public void totaleSconto() { 
		int conto = 0;
		for (int i = 0; i < reportVendite.size(); i++) {
			conto+=reportVendite.get(i).getSconto();
		}
		System.out.println("\nSconto totale: " + conto +"$.");
	}
}
