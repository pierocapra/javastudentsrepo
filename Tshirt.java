package corso.lez5.esercizioscarpe;

//Tshirt eredita da Articolo
public class Tshirt extends Articolo{

	private String logo;
	
	public Tshirt() {
		
	}
	
	public Tshirt(String logo, String varMarca, String varTaglia, String varTessuto, String varColore,
			String varGenere, int varQuantitaDisponibile, float varPrezzo) {
		super.marca = varMarca;
		super.taglia = varTaglia;
		super.tessuto = varTessuto;
		super.colore = varColore;
		super.genere = varGenere;
		super.prezzo = varPrezzo;
		super.quantitaDisponibile = varQuantitaDisponibile;
		this.logo = logo;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	

		//metodi Interni
		public void stampaTshirt() {
			System.out.println("\n-T-Shirt-\nMarca: " + this.marca +"\nLogo: " + this.logo +"\n"+ "Taglia: " + this.taglia
					+ "\n" + "Tessuto: " + this.tessuto + "\n" + "Colore: " + this.colore + "\n" + "Genere: " + this.genere
					+ "\n" + "Quantita' Disponibile: " + this.quantitaDisponibile + "\n" + "Prezzo: " + this.prezzo 
					+ "$\n"+ "Codice alfanumerico: " + this.codiceAlfaNum + "\n");
		}

		@Override
		public String toString() {
			return "Tshirt [marca=" + marca + ", logo=" + logo + ", taglia=" + taglia + ", tessuto=" + tessuto
					+ ", colore=" + colore + ", genere=" + genere + ", prezzo=" + prezzo + ", quantitaDisponibile="
					+ quantitaDisponibile + ", codiceAlfaNum=" + codiceAlfaNum + "]";
		}


		
}
