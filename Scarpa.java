package corso.lez5.esercizioscarpe;

//Scarpa eredita da Articolo
public class Scarpa extends Articolo{
	
	private String modello;
	private Accessori accessori;
	//Accessori e' una sottoclasse di Scarpa 
	
	public Scarpa() {
		
	}
	public Scarpa( String varModello, String varMarca, String varTaglia, String varTessuto, String varColore,
			String varGenere, int varQuantitaDisponibile, float varPrezzo) {
		
		super.marca = varMarca;
		super.taglia = varTaglia;
		super.tessuto = varTessuto;
		super.colore = varColore;
		super.genere = varGenere;
		super.prezzo = varPrezzo;
		super.quantitaDisponibile = varQuantitaDisponibile;
		
		this.modello = varModello;
		
	}
	
	//GETTERS AND SETTERS
		public void setModello(String varModello) {
			this.modello = varModello;
		}
		
		public String getModello() {
			return this.modello;
		}

		//metodi Interni
		public void stampaScarpa() {
			System.out.println("\n-Scarpa-\nMarca: " + this.marca + "\nModello: " + this.modello  + "\n" + "Taglia: " + this.taglia
					+ "\n" + "Tessuto: " + this.tessuto + "\n" + "Colore: " + this.colore + "\n" + "Genere: " + this.genere
					+ "\n" + "Quantita' Disponibile: " + this.quantitaDisponibile + "\n" + "Prezzo: " + this.prezzo 
					+ "$\n"+ "Codice alfanumerico: " + this.codiceAlfaNum + "\n");
		}
		
		public void aggiungiAccessori(boolean varSottopiedi, boolean varStringhe, boolean varCalzascarpe){
			Accessori accessoriDisponibili = new Accessori(varSottopiedi, varStringhe, varCalzascarpe);
			this.accessori = accessoriDisponibili;
		}
		
		public void stampaAccessori() {
			System.out.println("\nAccessori Disponibili per la "+this.marca+" "+this.modello+":" +this.accessori);
		}
		
		@Override
		public String toString() {
			return "Scarpa [marca=" + marca + ", modello=" + modello + ", taglia=" + taglia + ", tessuto=" + tessuto
					+ ", colore=" + colore + ", genere=" + genere + ", prezzo=" + prezzo + ", quantitaDisponibile="
					+ quantitaDisponibile + ", codiceAlfaNum=" + codiceAlfaNum + "]";
		}
		
		
		
		
}
