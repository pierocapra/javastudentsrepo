package corso.lez5.esercizioscarpe;

//Accessori e' una sottoclasse di Scarpa
public class Accessori {

	private boolean sottopiedi;
	private boolean stringhe;
	private boolean calzascarpe;
	
	public Accessori() {
		
	}
	public Accessori(boolean varSottopiedi, boolean varStringhe, boolean varCalzascarpe) {
		this.sottopiedi = varSottopiedi;
		this.stringhe = varStringhe;
		this.calzascarpe = varCalzascarpe;
	}
	
	//GETTERS AND SETTERS
	public boolean isSottopiedi() {
		return sottopiedi;
	}
	public void setSottopiedi(boolean sottopiedi) {
		this.sottopiedi = sottopiedi;
	}
	public boolean isStringhe() {
		return stringhe;
	}
	public void setStringhe(boolean stringhe) {
		this.stringhe = stringhe;
	}
	public boolean isCalzascarpe() {
		return calzascarpe;
	}
	public void setCalzascarpe(boolean calzascarpe) {
		this.calzascarpe = calzascarpe;
	}
	
	//to String
	public String toString() {
		String accessoriDisponibili = "";
		
		if (this.sottopiedi == true){
			accessoriDisponibili += "\n - Sottopiede disponibile\n";
		} else {
			accessoriDisponibili += "\n - Sottopiede non disponibile\n";
		}
		
		if (this.stringhe == true){
			accessoriDisponibili += " - Stringhe disponibili\n";
		} else {
			accessoriDisponibili += " - Stringhe non disponibili\n";
		}
		
		if (this.calzascarpe == true){
			accessoriDisponibili += " - calzascarpe disponibile\n";
		} else {
			accessoriDisponibili += " - calzascarpe non disponibile\n";
		}
		
		return accessoriDisponibili;
	}
}
