package corso.lez5.esercizioscarpe;

import java.util.ArrayList;
import java.util.List;

//inventario raggruppa tutti gli articoli in una lista e genera automaticamente un codice alfanumerico 
public class Inventario {

	private List<Articolo> inventario = new ArrayList<Articolo>();
	private int counterCodice = 1001;
	
	public void inserisci(InventarioSpecifico inventarioSpecifico, Articolo articolo) {
		
		if(articolo instanceof Scarpa) {
			articolo.setCodiceAlfaNum("SCA-"+counterCodice);			
		} else if (articolo instanceof Tshirt){
			articolo.setCodiceAlfaNum("TSH-"+counterCodice);
		} else if (articolo instanceof Cappello){
			articolo.setCodiceAlfaNum("CAP-"+counterCodice);
		}
		
		counterCodice++;
		
		this.inventario.add(articolo);
		inventarioSpecifico.inserisci(articolo);
	}
	
	public boolean rimuovi(Articolo articolo) {
		return this.inventario.remove(articolo);
	}
	
	public void rimuoviConCodice(String varCodice) {
		for (int i=0; i<inventario.size();i++) {
			String cod =inventario.get(i).getCodiceAlfaNum();
			if(cod.equals(varCodice)) {
				System.out.println("L'articolo codice "+ inventario.get(i).getCodiceAlfaNum()+" e' stato rimosso dall'inventario");
				this.inventario.remove(inventario.get(i));
			};
		}
	}
	
	public void stampaInventario() {
		System.out.println("\nInventario di tutti gli articoli\n");
		for (int i = 0; i<inventario.size();i++) {
			System.out.println(inventario.get(i));
		}
	}
	
	public void stampaArticoloConCodice(String varCodice) {	
		for (int i=0; i<inventario.size(); i++) {
//			System.out.println(inventario.get(i).getCodiceAlfaNum());
			if(inventario.get(i).getCodiceAlfaNum().equals(varCodice)) {
				System.out.println(inventario.get(i));
			};
		}
	}
	
	//CONTEGGI VARI 
	public void conteggio() {
		System.out.println("\nNel magazzino ci sono " + inventario.size() + " diversi articoli.");
	}
	
	public void conteggioPerGenere(String varCodice) {
		int conto = 0;
		for (int i=0; i<inventario.size();i++) {
			if(inventario.get(i).getGenere()== varCodice) {
				conto++;
			};
		}
		System.out.println("\nNel magazzino ci sono " + conto + " tipi di articoli femminili.");
	}
	
	public void conteggioQuantitaTotaleArticolo() {
		int conto = 0;
		for (int i=0; i<inventario.size();i++) {
			conto += inventario.get(i).getQuantitaDisponibile();
		}
		System.out.println("\nNel magazzino ci sono " + conto + " articoli in totale.");
	}
}
