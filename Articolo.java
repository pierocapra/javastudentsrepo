package corso.lez5.esercizioscarpe;

//Articolo e' la parent class 
public class Articolo {

	protected String marca;
	protected String taglia;
	protected String tessuto;
	protected String colore;
	protected String genere; // (maschile, femminile, unisex)
	protected float prezzo;
	protected int quantitaDisponibile;
	protected String codiceAlfaNum;
	
	public Articolo() {
		
	}

	public Articolo(String marca, String taglia, String tessuto, String colore, String genere, float prezzo,
			int quantitaDisponibile) {
		this.marca = marca;
		this.taglia = taglia;
		this.tessuto = tessuto;
		this.colore = colore;
		this.genere = genere;
		this.prezzo = prezzo;
		this.quantitaDisponibile = quantitaDisponibile;
		
	}


	public String toString() {
		return "Articolo [marca=" + marca + ", taglia=" + taglia + ", tessuto=" + tessuto + ", colore=" + colore
				+ ", genere=" + genere + ", prezzo=" + prezzo + ", quantitaDisponibile=" + quantitaDisponibile
				+ ", codiceAlfaNum=" + codiceAlfaNum + "]";
	}

	//GETTERS AND SETTERS
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getTaglia() {
		return taglia;
	}

	public void setTaglia(String taglia) {
		this.taglia = taglia;
	}

	public String getTessuto() {
		return tessuto;
	}

	public void setTessuto(String tessuto) {
		this.tessuto = tessuto;
	}

	public String getColore() {
		return colore;
	}

	public void setColore(String colore) {
		this.colore = colore;
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	public int getQuantitaDisponibile() {
		return quantitaDisponibile;
	}

	public void setQuantitaDisponibile(int quantitaDisponibile) {
		this.quantitaDisponibile = quantitaDisponibile;
	}

	public String getCodiceAlfaNum() {
		return codiceAlfaNum;
	}

	public void setCodiceAlfaNum(String varCodiceAlfaNum) {
		this.codiceAlfaNum = this.marca + "-" + varCodiceAlfaNum;
	}
	
	
}
