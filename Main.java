package corso.lez5.esercizioscarpe;

public class Main {

	public static void main(String[] args) {
		
		Inventario inventario  = new Inventario();
		InventarioSpecifico inventarioScarpe  = new InventarioSpecifico();
		InventarioSpecifico inventarioTshirt  = new InventarioSpecifico();
		InventarioSpecifico inventarioCappelli  = new InventarioSpecifico();
		ReportVendite reportVendite = new ReportVendite();
		
		////////////////////////INSERIMENTO ARTICOLI//////////////////
		//SCARPE
		Scarpa scarpa_1 = new Scarpa("Pump", "Reebok",  "41 1/2", "Gomma", "Aquamarine", "Maschile", 3, 45.5f);
		inventario.inserisci(inventarioScarpe, scarpa_1);
		Scarpa scarpa_2 = new Scarpa("Air", "Nike",  "40 1/2", "Fibra", "Black", "Unisex", 5, 80.9f);
		inventario.inserisci(inventarioScarpe, scarpa_2);
		Scarpa scarpa_3 = new Scarpa("Outdoor", "Timberland", "43", "Pelle", "Brown", "Maschile", 2, 34.5f);
		inventario.inserisci(inventarioScarpe, scarpa_3);
		Scarpa scarpa_4 = new Scarpa("Horse Riding", "Gucci", "37 1/3", "Pelle", "Light Brown", "Femminile", 4, 215.5f);
		inventario.inserisci(inventarioScarpe, scarpa_4);
		Scarpa scarpa_5 = new Scarpa("Air", "Nike", "38", "Plastic", "Rosa", "Femminile", 10, 95.5f);
		inventario.inserisci(inventarioScarpe, scarpa_5);
		Scarpa scarpa_6 = new Scarpa( "Fly", "Adidas", "43", "Gomma", "Black", "Unisex", 8, 49.9f);
		inventario.inserisci(inventarioScarpe, scarpa_6);
		scarpa_6.aggiungiAccessori(true, false, true);
		Scarpa scarpa_7= new Scarpa("Fly",  "Adidas", "41 3/4", "Plastic", "Green", "Maschile", 9, 139.50f);
		inventario.inserisci(inventarioScarpe, scarpa_7);
		scarpa_7.aggiungiAccessori(true, false, true);

		//Tshirt
		Tshirt tshirt_1 = new Tshirt("Smile","Reebok", "M", "Cotone", "Red", "Maschile", 3, 15.5f);
		inventario.inserisci(inventarioTshirt, tshirt_1);
		Tshirt tshirt_2 = new Tshirt("Runner","Adidas", "S", "Cotone", "Blue", "Femminile", 3, 29.9f);
		inventario.inserisci(inventarioTshirt, tshirt_2);
		Tshirt tshirt_3 = new Tshirt("Runner","Adidas", "L", "Sintetico", "Black", "Femminile", 3, 29.9f);
		inventario.inserisci(inventarioTshirt, tshirt_3);
		
		//Cappelli
		Cappello cap_1 = new Cappello("Reebok", "52", "Cotone", "Blue", "Unisex", 12, 25.5f);
		inventario.inserisci(inventarioCappelli, cap_1);
		Cappello cap_2 = new Cappello("NewHats", "54", "Sintetico", "Rosso", "Unisex", 10, 15.5f);
		inventario.inserisci(inventarioCappelli, cap_2);
		
		
		//////////////////VENDITE//////////////////////////
		
		Vendita vendita_1 = new Vendita("21/3/21", 0, scarpa_1);
		reportVendite.inserisciVendita(vendita_1);
		Vendita vendita_2 = new Vendita("14/4/21", 3.5f, scarpa_6);
		reportVendite.inserisciVendita(vendita_2);
		Vendita vendita_3 = new Vendita("16/4/21", 10f, tshirt_3);
		reportVendite.inserisciVendita(vendita_3);
		Vendita vendita_4 = new Vendita("18/4/21", 3.5f, cap_1);
		reportVendite.inserisciVendita(vendita_4);
		Vendita vendita_5 = new Vendita("19/4/21", 0f, cap_2);
		reportVendite.inserisciVendita(vendita_5);
		
		/////////////STAMPE E LOGICA/////////////////////////
		
		//rimuovere le // per testare le operazioni
		
//		scarpa_6.stampaScarpa();
//		scarpa_5.stampaAccessori();
//		tshirt_1.stampaTshirt();
//		cap_1.stampaCappello();

//		inventario.stampaInventario();
//		inventario.rimuovi(scarpa_4);
//		inventario.rimuoviConCodice("Reebok-SCA-1001");
//		inventario.stampaInventario();
		
//		inventarioScarpe.stampaInventarioSpecifico();
//		inventarioTshirt.stampaInventarioSpecifico();
//		inventarioCappelli.stampaInventarioSpecifico();
		
//		inventario.stampaArticoloConCodice("Reebok-CAP-1011");
//		reportVendite.stampaReportVendite();
//		reportVendite.totaleIncasso();
//		reportVendite.totaleSconto();
		
//		inventario.conteggio();
//		inventario.conteggioPerGenere("Femminile"); 
		
//		inventario.conteggioQuantitaTotaleArticolo();
		
		
		
		
	}

}
