package corso.lez5.esercizioscarpe;

//Cappello eredita da Articolo
public class Cappello extends Articolo{

	public Cappello() {
		
	}
	public Cappello(String varMarca, String varTaglia, String varTessuto, String varColore,
			String varGenere, int varQuantitaDisponibile, float varPrezzo) {
		
		super.marca = varMarca;
		super.taglia = varTaglia;
		super.tessuto = varTessuto;
		super.colore = varColore;
		super.genere = varGenere;
		super.prezzo = varPrezzo;
		super.quantitaDisponibile = varQuantitaDisponibile;
		
	}
	//metodi Interni
			public void stampaCappello() {
				System.out.println("\n-Cappello-\nMarca: " + this.marca + "\n" + "Taglia: " + this.taglia
						+ "\n" + "Tessuto: " + this.tessuto + "\n" + "Colore: " + this.colore + "\n" + "Genere: " + this.genere
						+ "\n" + "Quantita' Disponibile: " + this.quantitaDisponibile + "\n" + "Prezzo: " + this.prezzo 
						+ "$\n"+ "Codice alfanumerico: " + this.codiceAlfaNum + "\n");
			}
			
	@Override
	public String toString() {
		return "Cappello [marca=" + marca + ", taglia=" + taglia + ", tessuto=" + tessuto + ", colore=" + colore
				+ ", genere=" + genere + ", prezzo=" + prezzo + ", quantitaDisponibile=" + quantitaDisponibile
				+ ", codiceAlfaNum=" + codiceAlfaNum + "]";
	}

}
