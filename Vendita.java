package corso.lez5.esercizioscarpe;


//La classe vendita e' indipente da gli articoli a dagli inventari ma decrementa la quantita dell'articolo venduto
public class Vendita {

	private Articolo articolo;
	private float sconto;
	private float prezzo;
	private String data;
	private String articoloCode;

	public Vendita(String data, float sconto, Articolo articolo) {
		this.data = data;
		this.articolo = articolo;
		this.sconto = sconto;
		this.prezzo = articolo.getPrezzo() - this.sconto;
		this.articoloCode = articolo.getCodiceAlfaNum();
		
		int quantitaAttuale = articolo.getQuantitaDisponibile();
		articolo.setQuantitaDisponibile(quantitaAttuale - 1);
	}
	
	public void stampaVendita() {
		String marca = articolo.getMarca();
		String tipo = "";
		
		if(articolo instanceof Scarpa) {
			tipo = " e' stata venduta una Scarpa ";			
		} else if (articolo instanceof Tshirt){
			tipo = " e' stata venduta una Tshirt ";
		} else if (articolo instanceof Cappello){
			tipo = " e' stato venduto un Cappello ";
		}
		
		System.out.println("In data "+ this.data + tipo + marca +
				" con uno sconto di " + this.sconto + "$, al prezzo finale di "+ prezzo +"$.");
	}
	
	//GETTERS
	
	public Articolo getArticolo() {
		return articolo;
	}

	public void setArticolo(Articolo articolo) {
		this.articolo = articolo;
	}

	public String getArticoloCode() {
		return articoloCode;
	}

	public void setArticoloCode(String articoloCode) {
		this.articoloCode = articoloCode;
	}

	public void setSconto(float sconto) {
		this.sconto = sconto;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getData() {
		return this.data;
	}
	
	public float getPrezzo() {
		return this.prezzo;
	}
	
	public float getSconto() {
		return this.sconto;
	}
	
	public String getCode() {
		return this.articoloCode;
	}
}
