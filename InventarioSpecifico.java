package corso.lez5.esercizioscarpe;

import java.util.ArrayList;

//Inventario Specifico raggruppa automaticamente gli articoli di stesso tipo
public class InventarioSpecifico {

	private ArrayList<Articolo> inventario = new ArrayList<Articolo>();
	
	public void inserisci(Articolo articolo) {
		this.inventario.add(articolo);
	}
	
	public boolean rimuovi(Articolo articolo) {
		return this.inventario.remove(articolo);
	}

	public void stampaInventarioSpecifico() {
		System.out.println("\nInventario per categoria\n");
		for (int i = 0; i<inventario.size();i++) {
			System.out.println(inventario.get(i));
		}
	}
}
